$Id$

Без патчей модуль работает, но результатов работы никаких не видно. Поэтому, лучше всего эти патчи установить. Они предназначены для Drupal 4.4. Ниже см. описание технологии изменения модуля для работы с term_access. Как минимум, следует пропатчить модули taxonomy и node.


* Список патчей по модулям

Легенда:
x - модулю патчи не требуются
+ - патч готов
- - не готов
* - в процессе подготовки

Стандартые модули Drupal:

x admin
x aggregator
+ archive
x block
+ blog
+ blogapi
- book
x comment
x drupal
x filter
* forum
x help
x locale
+ node
x path
+ page
x ping
+ poll
x profile
- queue
x search
- statistics
x story
x system
+ taxonomy
x throttle
+ title
+ tracker
x user
x watchdog

Сторонние модули (также все для 4.4):

* image
* weblink

Остальные модули будут патчиться по мере надобности. Помощь приветствуется.

Применить сразу все патчи можно например так:

$ cp -r patches $КОРНЕВАЯ_ДИРЕКТОРИЯ_DRUPAL
$ cd $КОРНЕВАЯ_ДИРЕКТОРИЯ_DRUPAL
$ for i in patches/*; do patch -p0 <$i; done;


* Изменения модулей для работы с term_access

Term_access предлагает простую проверку - он возвращает набор элементов таксономии (terms) разрешенных для просмотра данным пользователем. Т.е. чтобы защитить документы (nodes) от просмотра пользователями определенных ролей, надо добавить в запросы и прочие места, где происходит работа с документами и элементами таксономии проверку предлагаемую term_access.

Функции term_access для осуществления проверок:

function term_access_get_terms($global = NULL) - возвращает массив из tid доступных текущему пользователю, или uid заданному $global

function term_access_get_terms_string($global = NULL) - то же самое, но возвращается строка из tid (для удобства вставки в SQL)

function term_access_node_access($nid, $taxonomy=NULL) - проверка прав доступа пользователя к документу (если не указан массив элементов таксономии во втором аргументе - данные берутся из базы), возвращает истину если проверка успешна

function term_access_and_check($nid, $taxonomy=NULL) - аналогично, но для режима OR всегда возвращает истину, реальная проверка осуществляется только если текущий режим работы модуля - AND

Общая техника: следует искать в модуле запросы SELECT работающие с таблицей node и добавлять в них проверку. Вот как выглядит патч для большинства запросов:

// оригинальный запрос
"SELECT nid, type FROM {node} WHERE promote = 1 AND status = 1 ORDER BY static DESC, created DESC"

// после патча
"SELECT DISTINCT n.nid, n.type FROM {node} n LEFT JOIN {term_node} t ON n.nid = t.nid WHERE (t.tid IN (". term_access_get_terms_string() .") OR t.tid IS NULL) AND n.promote = 1 AND n.status = 1 ORDER BY n.static DESC, n.created DESC"

Т.е. добавлено соединение с таблицей term_node и проверка tid на допустимые значения (они формируются вызовом функции term_access_get_terms_string), также проверяются документы не привязанные вовсе к таксономии (tid IS NULL). Необходимость добавления DISTINCT обусловлена LEFT JOIN (вместо INNER JOIN), поскольку при написании модуля было принято, что документы не привязанные к тасономии должны показываться всем пользователям. Отсюда вылез LEFT JOIN и проверка на NULL потянувшие за собой DISTINCT. Если кто знает как можно проще - подскажите?

Для некоторых запросов однако требуется INNER JOIN и без проверки tid IS NULL, тогда DISTINCT тоже не нужен:

SELECT ... INNER JOIN {term_node} t ON n.nid = t.nid WHERE t.tid IN (". term_access_get_terms_string() .") AND ...

Для сравнения, в предыдущем варианте выше было:

SELECT DISTINCT ...
LEFT JOIN {term_node} t ON n.nid = t.nid WHERE (t.tid IN (". term_access_get_terms_string() .") OR t.tid IS NULL) AND ...

Также, для того чтобы при отключении модуля движок не падал по причине неожиданной потери функции term_access_get_terms_string запросы обрамляются в следующую конструкцию:

  if (function_exists("term_access_get_terms_string")) {
	... patched stuff ...
  }
  else {
      	... original stuff ...
  }

Это опционально и можно так не делать, но тогда не отключайте модуль term_access без предварительного отката всех патчей! Использована function_exists вместо предлагаемой Drupal для проверки наличия модуля module_exist - поскольку первая функция работает быстрей (такое мнение видел в drupal-devel и похоже так оно и есть).

Остальные примерчики можно посмотреть в патчах.
