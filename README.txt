
TERM ACCESS for 4.4

Module offers access to nodes based on taxonomy terms. Terms associates with user roles and may be visible or unvisible for selected roles.

If node has no bindings to taxonomy terms - it visible to all users. If more than one term bind to node, by default works logical OR condition (access to one of terms means access to node). This mode may be changed in module preferences to logical AND condition (must be accessible all terms for getting access to node).

NOTE: logical AND mode very uncomplete now and supported not by all patches for modules
NOTE#2: OR mode makes lesser queries to DB and works faster
NOTE#3: term_access still in development and may do strange things sometimes.

Module not separate conditions on "create", "modify" and so on - only offers visibility or unvisibility for terms and nodes. It is satisfactory for many cases, but if functionality is insufficient - better to use more feature rich modules like 'groups' or 'taxonomy_access'. Positive moment of such simplicity - module is very short (4.5K).

By ergonomic considerations assigning roles to terms occurs in form of creating or editing terms (that not common for Drupal, where all permissions usually sets on 'permissions' page). Now I think this is a better interface, but may be I wrong... Then change it later to standard Drupal way.

Was used some ideas from 'groups' (by Gerhard Killesreiter) and 'taxonomy_access' (by Moshe Weitzman) modules.

Term_access require some patching of Drupal 4.4 modules. See PATCHES for more info. Module was written and tested for Drupal 4.4.1.

License (GPL) - see LICENSE
Module installation/uninstallation - see doc/INSTALL
Patches - see doc/PATCHES
Whats about future? - doc/TODO
Bugs - doc/BUGS

More detailed docs supplied for Russian-spoken users - see doc/*ru files.

Bugreports & suggestions send to Axel <axel@drupal.ru>.
