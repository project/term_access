--
-- term_access module
-- Table structures (PostgreSQL)
-- Don't forget to set appropriate table prefix!
--

-- Table for binding terms to roles
CREATE TABLE IF NOT EXISTS term_access (
  tid int(10) NOT NULL default '0',
  rid int(10) NOT NULL default '0',
  KEY tid (tid),
  KEY rid (rid)
);

-- Table for storage default roles assigned to new terms
CREATE TABLE IF NOT EXISTS vocabulary_access (
  vid int(10) NOT NULL default '0',
  rid int(10) NOT NULL default '0',
  KEY vid (vid),
  KEY rid (rid)
);
